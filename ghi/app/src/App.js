import React, { useState, useEffect } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoesList from './ShoesList';
import BinForm from './BinForm';
import BinsList from './BinsList';
import HatForm from './HatForm';
import HatList from './HatList';
import HatDetail from './HatDetail';

function App() {
  const [shoes, setShoes] = useState([])
  const [bins, setBins] = useState([])
  const [hats, setHats] = useState([])

  const getShoes = async () => {
    const shoeUrl = 'http://localhost:8080/api/shoes'
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }

  const getBins = async () => {
    const binUrl = 'http://localhost:8100/api/bins'
    const binResponse = await fetch(binUrl);

    if (binResponse.ok) {
      const data = await binResponse.json();
      const bins = data.bins
      setBins(bins)
    }
  }

  const getHats = async () => {
    const hatUrl = 'http://localhost:8090/hats'
    const hatResponse = await fetch(hatUrl);
    if (hatResponse.ok) {
      const data = await hatResponse.json();
      const hats = data.hats
      setHats(hats)
    }
  }

  useEffect( () => {
    getShoes();
    getBins();
    getHats();
  }, [
    setShoes,
    setBins,
    setHats,
  ]
  )

  if (shoes === undefined && bins === undefined) {
    return null;
  }


  if (hats === undefined) {
    return null;
  }



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={shoes} getShoes={getShoes} />} />
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
          </Route>
          <Route path="bins">
            <Route path="" element={<BinsList bins={bins} getBins={getBins} />} />
            <Route path="new" element={<BinForm getBins={getBins}/>} />
          </Route>
          <Route path="/hats" element={<HatList hats={hats} />} />
          <Route path="/hats/:hatId" element={<HatDetail hats={hats} />} />
          <Route path="/hats/new" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
