window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const hatSelectTag = document.getElementById('hat');
        data.hats.forEach(hat => {
            const option = document.createElement('option');
            option.value = hat.id;
            option.textContent = hat.name;
            hatSelectTag.appendChild(option);
        });
    }
    const formTag = document.getElementById('create-hat-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const hatURL = 'http://localhost:8000/api/hats/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
    });
});
