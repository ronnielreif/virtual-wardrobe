# Generated by Django 4.0.3 on 2024-02-04 04:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_remove_locationvo_closet_name_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='closet_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
