# Wardrobify

Team:

* Ronnie - Hats
* Jonathan - Shoes

## Design

## Shoes microservice

Started by creating the shoe model to grab an empty list in insomnia, then building it out along with enconders I knew I was going to need.

## Hats microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->

Start with creating the backend with a simple model and view. Ensure it all works together
then put in the specifics. Once that's done I'll switch to doing the frontend.
